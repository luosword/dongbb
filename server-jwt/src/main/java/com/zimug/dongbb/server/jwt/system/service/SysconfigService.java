package com.zimug.dongbb.server.jwt.system.service;

import com.zimug.dongbb.common.persistence.auto.mapper.SysConfigMapper;
import com.zimug.dongbb.common.persistence.auto.model.SysConfig;
import com.zimug.dongbb.common.persistence.auto.model.SysConfigExample;
import com.zimug.dongbb.common.utils.exception.CustomException;
import com.zimug.dongbb.common.utils.exception.CustomExceptionType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class SysconfigService {

  @Resource
  private SysConfigMapper sysConfigMapper;

  public List<SysConfig> queryConfigs(String configLik) {
      SysConfigExample sysConfigExample = new SysConfigExample();
      if(StringUtils.isNotEmpty(configLik)){
        sysConfigExample.or().andParamNameLike("%"+ configLik+ "%");
        sysConfigExample.or().andParamKeyLike("%"+ configLik+ "%");
      }
      return sysConfigMapper.selectByExample(sysConfigExample);
  }

  public void updateConfig(SysConfig sysconfig){
    if(sysconfig.getId() == null){
      throw new CustomException(CustomExceptionType.USER_INPUT_ERROR,
        "修改操作必须带主键");
    }else{
      sysConfigMapper.updateByPrimaryKeySelective(sysconfig);
    }
  }

  public void addConfig(SysConfig sysconfig){
    sysconfig.setCreateTime(new Date());
    sysConfigMapper.insert(sysconfig);
  }

  public void deleteConfig(Integer configId){
    sysConfigMapper.deleteByPrimaryKey(configId);
  }

}
