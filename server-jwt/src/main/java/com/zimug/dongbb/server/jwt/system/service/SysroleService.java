package com.zimug.dongbb.server.jwt.system.service;

import com.zimug.dongbb.common.persistence.auto.mapper.SysRoleMapper;
import com.zimug.dongbb.common.persistence.auto.mapper.SysUserRoleMapper;
import com.zimug.dongbb.common.persistence.auto.model.SysRole;
import com.zimug.dongbb.common.persistence.auto.model.SysRoleExample;
import com.zimug.dongbb.common.persistence.auto.model.SysUserRoleExample;
import com.zimug.dongbb.common.persistence.system.mapper.SystemMapper;
import com.zimug.dongbb.common.utils.exception.CustomException;
import com.zimug.dongbb.common.utils.exception.CustomExceptionType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SysroleService {

  @Resource
  private SysRoleMapper sysRoleMapper;
  @Resource
  private SystemMapper systemMapper;
  @Resource
  private SysUserRoleMapper sysUserRoleMapper;

  public List<SysRole> queryRoles(String roleLik) {
      SysRoleExample sysRoleExample = new SysRoleExample();
      if(StringUtils.isNotEmpty(roleLik)){
        sysRoleExample.or().andRoleCodeLike("%"+ roleLik+ "%");
        sysRoleExample.or().andRoleDescLike("%"+ roleLik+ "%");
        sysRoleExample.or().andRoleNameLike("%"+ roleLik+ "%");
      }
      return sysRoleMapper.selectByExample(sysRoleExample);
  }
  public void updateRole(SysRole sysrole){
    if(sysrole.getId() == null){
      throw new CustomException(CustomExceptionType.USER_INPUT_ERROR,
        "修改操作必须带主键");
    }else{
      sysRoleMapper.updateByPrimaryKeySelective(sysrole);
    }
  }
  public void addRole(SysRole sysrole){
    sysRoleMapper.insert(sysrole);
  }
  public void deleteRole(Integer roleId){
    sysRoleMapper.deleteByPrimaryKey(roleId);
  }

  public Map<String,Object> getRolesAndChecked(Integer userId){
    Map<String,Object> ret = new HashMap<>();
    if(userId == null){
      throw new CustomException(CustomExceptionType.USER_INPUT_ERROR,
        "获取角色信息必需要：用户id参数");
    }else{
      ret.put("checkedRoleIds",systemMapper.getCheckedRoleIds(userId));
      ret.put("roleDatas",sysRoleMapper.selectByExample(null));
    }
    return ret;
  }


  @Transactional
  public void saveCheckedKeys(Integer userId,List<Integer> checkedIds){

    SysUserRoleExample sysUserRoleExample = new SysUserRoleExample();
    sysUserRoleExample.createCriteria().andUserIdEqualTo(userId);
    sysUserRoleMapper.deleteByExample(sysUserRoleExample);

    systemMapper.insertUserRoleIds(userId,checkedIds);
  }
}
